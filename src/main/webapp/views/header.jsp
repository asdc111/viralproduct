<header>
	<nav
		class="hidden-sm hidden-xs navbar navbar-default navbar-fixed-top sheet_open_blurred"
		id="top_nav" role="navigation">
		<div class="container-fluid">
			<ul class="navbar-nav navbar-right nav text-uppercase">
				<li><a id="user_login_btn" href="/users/sign_in">Log In</a></li>
				<li class="divider"></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"> <span class="caret"></span>
				</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/get-started">Sign Up</a></li>
						<li><a class="request_demo_link" data-toggle="sheet"
							data-target="#request-demo-sheet" href="#">Request Demo</a></li>
						<li><a href="/leaderboard">Leaderboard</a></li>
					</ul></li>
			</ul>
		</div>
	</nav>



</header>