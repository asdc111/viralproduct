

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Survey</title>
<script src="/viralproduct/js/jquery-1.8.3.js"></script>
<script src="/viralproduct/js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="/viralproduct/js/jquery.ajaxQueue.js"></script>
<script src="/viralproduct/js/survey.js"></script>
</head>
<body style="background-color: white;">
	<div id="form" style="height: 90%;">
		<h2>Please answer the following questions:</h2>
		<!-- <form id = "survey_form" action="submitAnswers()"> -->
		<ul>
			<li>
				<fieldset>
					<legend>
						<span>Which medium?</span>
					</legend>
					<label> <input type="radio" id="answer_FB" value="Facebook"
						name="answer1">Facebook
					</label> <label> <input type="radio" id="answer_Goog"
						value="Google+" name="answer1">Google+
					</label> <label> <input type="radio" id="answer_Twitter"
						value="Twitter" name="answer1">Twitter
					</label>

				</fieldset>

			</li>

			<li style="padding-top: 2%;">
				<fieldset>
					<legend>
						<span>How many posts or tweets do you need?</span>
					</legend>
					<label><input type="text" id="answer2"
						style="width: 65%; height: 5%;"></label>
				</fieldset>

			</li>

			<li style="padding-top: 2%;">
				<fieldset>
					<legend>
						<span>What should be the content of the tweet?</span>
					</legend>
					<label><input type="text" id="answer3"
						style="width: 65%; height: 5%;"></label>
				</fieldset>

			</li>
		</ul>
		<div id="submitbar"
			style="padding-top: 10%; text-align: right; height: 10%;">
			<input type="button" id="submitform" value="Submit"
				style="width: 10%; height: 10%;">
		</div>
		<!-- </form> -->
	</div>
</body>
</html>