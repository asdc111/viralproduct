<nav class="footer_nav" id="footer_mobile_menu" role="navigation">
	<div class="container-fluid">
		<ul class="navbar-nav navbar-right nav text-uppercase">
			<li class="dropdown">
			<a class="dropdown-toggle"
				data-toggle="dropdown" href="#">
					<div class="niche-marketing-icon menu-mobile-white size-30"></div>
			</a>
				<ul class="dropdown-menu" role="menu">
					<li><a class="action" href="/get-started">Sign Up</a></li>
					<li><a class="primary" id="user_login_btn"
						href="/users/sign_in">Log In</a></li>
					<li data-menuanchor="creators"><a data-target="#creators"
						aria-expanded="true" aria-controls="#creators" class="active"
						href="#creators">Creators</a></li>
					<li data-menuanchor="brands"><a data-target="#brands"
						aria-expanded="true" aria-controls="#brands" href="#brands">Brands</a>
					</li>
					<li data-menuanchor="about"><a data-target="#about"
						aria-expanded="true" aria-controls="#about�" href="#about">About</a>
					</li>
					<li><a data-toggle="sheet" data-target="#request-demo-sheet"
						href="#">Request Demo</a></li>
					<li><a target="_blank" href="http://blog.niche.co">Blog</a></li>
				</ul></li>

		</ul>
	</div>
</nav>