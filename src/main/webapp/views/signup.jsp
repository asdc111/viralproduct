
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>

<style>
.error {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #31708f;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#login-box {
	width: 300px;
	padding: 20px;
	margin: 100px auto;
	background: #fff;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border: 1px solid #000;
}
</style>
</head>
<body>
Publisher Sign Up 


<form class="publisher_open_id_auth" action="authsign" method="POST">
	<div class="pub-reg-email">
		<label class="reg-email-text">Email Address</label>
		<div class="inp err-po">
			<input class="authInput inp" id="inputEmail" name="email" placeholder="e.g. abc@xyz.com" type="text">
			<span class="BackendAfferror" style=""></span>
		</div>
	</div>
		<div class="pub-reg-pass">
		<label class="reg-email-text">Password</label>
		<div class="inp err-po">
			<input class="authInput inp" id="inputPass" name="pass" placeholder="password" type="password">
			<span class="BackendAfferror" style=""></span>
		</div>
	</div>
		<div class="pub-reg-confirm-pass">
		<label class="reg-conf-pass-text">Confirm password</label>
		<div class="inp err-po">
			<input class="authInput inp" id="inputConfPass" name="conf_pass" placeholder="Confirm password" type="password">
			<span class="BackendAfferror" style=""></span>
		</div>
	</div>
 <input type="hidden" 
                     name="${_csrf.parameterName}" value="${_csrf.token}" />
	<button class="loginbtn pubbtncutm modilogin" id="registerBtn" style="display: block;margin-top: 20px;">Register</button>

</form>
</body>
</html>