package com.viralproject.utils;

public class Pair {
	
	private String a;
	private String b;
	
	public Pair(String ar, String br)
	{
		a=ar;
		b=br;
	}
	
	public String getA()
	{
		return a;
	}
	
	public String getB()
	{
		return b;
	}
	
	public void setA(String ae)
	{
		a=ae;
	}
	
	public void setB(String ae)
	{
		b=ae;
	}
	
}
