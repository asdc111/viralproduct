package com.viralproduct.redistemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository("RedisTemplateOps")
public class RedisTemplateOps {

	@Autowired
	private RedisTemplate<String, String> redisTemplateOps;

	public String getValueByKey(String key) {
		// TODO Auto-generated method stub
		return redisTemplateOps.opsForValue().get(key);

	}

	public void setValueByKey(String key, String value) {
		// TODO Auto-generated method stub

		redisTemplateOps.opsForValue().set(key, value);

	}

	public boolean isExistByKey(String key) {
		// TODO Auto-generated method stub
		redisTemplateOps.convertAndSend("pubsub", "loda");
		return redisTemplateOps.hasKey(key);
	}

	public boolean publishMessageUsingChannelName(String channel, String message) {
		// TODO Auto-generated method stub
		redisTemplateOps.convertAndSend(channel, message);
		return false;
	}
	
	
	
}
