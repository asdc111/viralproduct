package com.viralproduct.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viralproduct.security.custom.entity.ForgotPasswordEntity;
import com.viralproduct.security.custom.entity.NotificationEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.custom.entity.UserInfoEntity;
import com.viralproduct.security.custom.entity.UserSocialAccountEntity;

@Repository("authUserDao")
public class AuthUserDaoImpl implements AuthUserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public UserEntity getUserByEmail(String email) {
		// TODO Auto-generated method stub
		String getAuthUserQuery = "select p from UserEntity p where email_id=:emailId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(getAuthUserQuery);
		query.setParameter("emailId", email);
		UserEntity userEnt = (UserEntity) query.uniqueResult();
		return userEnt;
	}

	@Override
	public UserEntity getUserByUserId(String UserId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity saveUserByEmail(String Email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity saveUserByEmailAndSocialInfo(
			UserSocialAccountEntity socialInfo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserInfoEntity getUserInfo(UserInfoEntity userInfoEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserInfoEntity saveUserInfo(UserInfoEntity userInfoEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity isUserExistByEmailId(String emailId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity createAndUpdateAuthUser(UserEntity userEntity) {
		// TODO Auto-generated method stub

		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(userEntity);

		return userEntity;

	}

	@Override
	public UserSocialAccountEntity getOrCreateSocialUser(
			UserSocialAccountEntity userSocialAccountEntity) {
		String getAuthUserQuery = "select p from UserSocialAccountEntity  p where uid=:uid and providersType=:provider_type";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(getAuthUserQuery);
		query.setParameter("uid", userSocialAccountEntity.getUid());
		query.setParameter("provider_type",
				userSocialAccountEntity.getProvidersType());
		UserSocialAccountEntity userSocialAccountEnt = (UserSocialAccountEntity) query
				.uniqueResult();
		if (userSocialAccountEnt == null) {
			session.save(userSocialAccountEntity);
			return userSocialAccountEntity;
		}

		return userSocialAccountEnt;
	}

	@Override
	public UserEntity saveAuthUser(UserEntity userEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void forgotPasswordSaveInDb(ForgotPasswordEntity forgotPasswordEntity) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(forgotPasswordEntity);
	}

	@Override
	public ForgotPasswordEntity getforgotPasswordEntity(String hash) {
		String getAuthUserQuery = "select p from ForgotPasswordEntity p where generated_hash=:generatedHash";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(getAuthUserQuery);
		query.setParameter("generatedHash", hash);
		ForgotPasswordEntity forgotPasswordEntity = (ForgotPasswordEntity) query
				.uniqueResult();
		return forgotPasswordEntity;
	}

	@Override
	public void deletGeneratedHashByEmailId(String email) {
		String getAuthUserQuery = "delete  from ForgotPasswordEntity  where email_id=:emailId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(getAuthUserQuery);
		query.setParameter("emailId", email);
		query.executeUpdate();

	}

	@Override
	public void saveNotification(NotificationEntity notificationEntity) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(notificationEntity);
	}

}
