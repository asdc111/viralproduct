package com.viralproduct.dao;


import javax.transaction.Transactional;

import com.viralproduct.security.custom.entity.ForgotPasswordEntity;
import com.viralproduct.security.custom.entity.NotificationEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.custom.entity.UserInfoEntity;
import com.viralproduct.security.custom.entity.UserSocialAccountEntity;


@Transactional
public interface AuthUserDao {

	UserEntity getUserByEmail(String email);
	UserEntity getUserByUserId(String UserId);
	UserEntity saveUserByEmail(String Email);
	UserEntity saveUserByEmailAndSocialInfo(UserSocialAccountEntity socialInfo);
	UserInfoEntity getUserInfo(UserInfoEntity userInfoEntity);
	UserInfoEntity saveUserInfo(UserInfoEntity userInfoEntity);
	UserEntity isUserExistByEmailId(String emailId);
	UserSocialAccountEntity getOrCreateSocialUser(UserSocialAccountEntity userSocialAccountEntity);
	UserEntity saveAuthUser(UserEntity userEntity);
	UserEntity createAndUpdateAuthUser(UserEntity userEntity);
	void forgotPasswordSaveInDb(ForgotPasswordEntity forgotPasswordEntity);
	ForgotPasswordEntity getforgotPasswordEntity(String hash);
	void deletGeneratedHashByEmailId(String email);
	void saveNotification(NotificationEntity notificationEntity);
}
