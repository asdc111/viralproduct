package com.viralproduct.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viralproduct.redistemplate.RedisTemplateOps;
import com.viralproduct.service.CacheService;

@Service("cacheService")
public class CacheServiceImpl implements CacheService {

	@Autowired
	private RedisTemplateOps redisTemplat;

	@Override
	public String getValue(String Key) {
		return redisTemplat.getValueByKey(Key);

	}

	@Override
	public void setValue(String Key, String Value) {
		redisTemplat.setValueByKey(Key, Value);

	}

	@Override
	public boolean isExist(String Key) {
		return redisTemplat.isExistByKey(Key);

	}

	@Override
	public boolean publishMessageUsingChannelName(String channel, String message) {
		return redisTemplat.publishMessageUsingChannelName(channel, message);
	}
}
