package com.viralproduct.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viralproduct.dao.AuthUserDao;
import com.viralproduct.security.custom.entity.ForgotPasswordEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.custom.entity.UserSocialAccountEntity;
import com.viralproduct.service.UserAuthService;

@Service("userAuthService")
@Transactional
public class UserAuthServiceImpl implements UserAuthService{

	@Autowired
	private AuthUserDao authUserDao;

	@Override
	public UserEntity createAnUpdateAuthUser(UserEntity userEntity) {
		
		return authUserDao.createAndUpdateAuthUser(userEntity);
	}

	@Override
	public UserSocialAccountEntity getOrCreateSocialUser(
			UserSocialAccountEntity userSocialAccountEntity) {
		return authUserDao.getOrCreateSocialUser(userSocialAccountEntity);

	}

	@Override
	public UserEntity getUserByEmail(String email) {
		// TODO Auto-generated method stub
		return authUserDao.getUserByEmail(email);
	}

	@Override
	public void forgotPasswordSaveInDb(ForgotPasswordEntity forgotPasswordEntity) {
		// TODO Auto-generated method stub
		authUserDao.forgotPasswordSaveInDb(forgotPasswordEntity);
		
	}



	@Override
	public ForgotPasswordEntity getforgotEntityByHash(String hash) {
		return authUserDao.getforgotPasswordEntity(hash);
		
	}

	@Override
	public void deletGeneratedHashByEmailId(String email) {
		authUserDao.deletGeneratedHashByEmailId(email);
		
	}



	
	
}
