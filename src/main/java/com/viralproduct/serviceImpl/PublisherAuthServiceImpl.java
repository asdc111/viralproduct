package com.viralproduct.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viralproduct.dao.AuthUserDao;
import com.viralproduct.security.custom.entity.NotificationEntity;
import com.viralproduct.service.PublisherService;



@Service("publisherService")
@Transactional
public class PublisherAuthServiceImpl implements PublisherService {


	@Autowired
	private AuthUserDao authUserDao;
	
	@Override
	public void saveNotObject(NotificationEntity notificationEntity) {
		// TODO Auto-generated method stub
		
		authUserDao.saveNotification(notificationEntity);
		
	}

	
}
