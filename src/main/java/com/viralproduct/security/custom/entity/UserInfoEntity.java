package com.viralproduct.security.custom.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "auth_info")
public class UserInfoEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1427085189651671931L;

	@Id @GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@OneToOne
    private UserEntity user;
	
	private String firstName;
	private String lastName;
	private Integer age;
	private Integer follower;
	private Integer followed;
	private Integer phoneNumber;
	
	
}
