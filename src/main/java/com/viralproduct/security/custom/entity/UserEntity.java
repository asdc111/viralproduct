package com.viralproduct.security.custom.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.viralproduct.security.constants.UserType;



@Entity
@Table(name = "auth_user")
public class UserEntity implements Serializable {
	
	/**
	 */
	
	private static final long serialVersionUID = 4652505746656505913L;
	@Id @GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(columnDefinition="VARCHAR(60)",name = "email_id") 
	private String emailId;
	
	@Column(name = "password") 
	private String password;

	
	@Column(name = "user_role")
	private String hasRole;
	
	@Column(name = "has_email")
	private boolean hasEmail;
	
	private Timestamp joinedDate;
	private Timestamp lastLogin;
	
	@Column(name = "user_name")
	private String userName;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId.toString();
	}
	@Column(name = "user_id")
	private String userId;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public boolean isHasEmail() {
		return hasEmail;
	}
	public void setHasEmail(boolean hasEmail) {
		this.hasEmail = hasEmail;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


	public String getHasRole() {
		return hasRole;
	}
	public void setHasRole(String hasRole) {
		this.hasRole = hasRole;
	}
	public Timestamp getJoinedDate() {
		return joinedDate;
	}
	public void setJoinedDate(Timestamp timeStamp) {
		this.joinedDate = timeStamp;
	}
	public Timestamp getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}


}
