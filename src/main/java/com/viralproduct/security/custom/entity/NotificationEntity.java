package com.viralproduct.security.custom.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.soap.Text;

@Entity
@Table(name = "notification")
public class NotificationEntity {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	public String getSocialType() {
		return socialType;
	}


	public void setSocialType(String string) {
		this.socialType = string;
	}


	@Column(name="number_tweet_post")
	private Integer numberOfTweetAndPost;
	
	
	@Column(name="content")
	private String Content;
	
	@Column(name="social_type")
	private String socialType;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getNumberOfTweetAndPost() {
		return numberOfTweetAndPost;
	}


	public void setNumberOfTweetAndPost(Integer numberOfTweetAndPost) {
		this.numberOfTweetAndPost = numberOfTweetAndPost;
	}


	public String getContent() {
		return Content;
	}


	public void setContent(String content) {
		Content = content;
	}
	
	
}

