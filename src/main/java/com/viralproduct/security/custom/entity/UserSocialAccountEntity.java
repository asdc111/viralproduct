package com.viralproduct.security.custom.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.viralproduct.security.constants.ProvidersType;


@Entity
@Table(name = "user_social_info")
public class UserSocialAccountEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4200450749720652272L;

	@Id @GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@OneToOne
    private UserEntity user;
	
	@Column(name = "uid")
	private Long uid;
	
	@Column(name = "access_token")
	private String accessToken ;
	
	@Column(name = "user_sekret_key")
	private String userSecretKey;
	
	@Column(name = "provider_type")
	private String providersType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public Long getUid() {
		return uid;
	}

	public void setUid(Long uid) {
		this.uid = uid;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserSecretKey() {
		return userSecretKey;
	}

	public void setUserSecretKey(String userSecretKey) {
		this.userSecretKey = userSecretKey;
	}

	public String getProvidersType() {
		return providersType;
	}

	public void setProvidersType(String string) {
		this.providersType = string;
	}

	
	
}
