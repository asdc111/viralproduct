package com.viralproduct.security.custom;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.viralproduct.dao.AuthUserDao;
import com.viralproduct.security.constants.ProvidersType;
import com.viralproduct.security.constants.UserType;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.custom.entity.UserSocialAccountEntity;
import com.viralproduct.service.UserAuthService;

import twitter4j.User;
import twitter4j.auth.AccessToken;

public class TwitterAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserAuthService userAuthService;

	@Autowired
	private CustomAuthentication customAuthentication;

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		// TODO Auto-generated method stub
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		return new UsernamePasswordAuthenticationToken(username, password,
				authentication.getAuthorities());

	}

	public boolean customAuthenticationMethod(twitter4j.User user,
			Authentication authentication, AccessToken accessToken) {

		UserEntity userEntity = new UserEntity();
		userEntity.setEmailId(null);
		userEntity.setHasEmail(false);
		userEntity.setHasRole(UserType.USER.code());
		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		userEntity.setJoinedDate(timeStamp);
		userEntity.setLastLogin(timeStamp);
		userEntity.setPassword(customAuthentication.makeMd5Hash(
				timeStamp.getTime(), Long.toString(user.getId())));
		userEntity.setUserName(user.getScreenName());
		userEntity = userAuthService.createAnUpdateAuthUser(userEntity);

		UserSocialAccountEntity userSocialAccountEntity = new UserSocialAccountEntity();
		userSocialAccountEntity.setUid(user.getId());
		userSocialAccountEntity.setAccessToken(accessToken.getToken());
		userSocialAccountEntity.setUser(userEntity);
		userSocialAccountEntity.setUserSecretKey(accessToken.getTokenSecret());
		userSocialAccountEntity.setProvidersType(ProvidersType.TWITTER.code());
		userAuthService.getOrCreateSocialUser(userSocialAccountEntity);
		this.authenticate(authentication);
		return true;

	}

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return false;
	}

}
