package com.viralproduct.security.custom;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.viralproduct.dao.AuthUserDao;
import com.viralproduct.security.custom.entity.ForgotPasswordEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.excep.SecurityExcepCustom;
import com.viralproduct.service.UserAuthService;

@Component("customAuthenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

	/*
	 * @Autowired private UserService userService;
	 */
	@Autowired
	private UserAuthService userAuthService;

	@Autowired
	private CustomAuthentication customAuthentication;

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();
		
		return new UsernamePasswordAuthenticationToken(username, password,
				authentication.getAuthorities());
	}

	public SecurityExcepCustom authenticate_signUp(String username,
			String pass, String confPass, String role) {
		UserEntity userEntity = userAuthService.getUserByEmail(username);
		SecurityExcepCustom securityException = new SecurityExcepCustom();
		if (userEntity != null) {
			securityException.setHasError(true);
			securityException.setResponseCode(1000);
			securityException.setError("User  found with this email");
			return securityException;
		}
		if (pass == confPass) {
			securityException.setHasError(true);
			securityException.setResponseCode(1100);
			securityException.setError("Password mismatchedd");
		}
		userEntity = new UserEntity();
		userEntity.setEmailId(username);
		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		userEntity.setJoinedDate(timeStamp);
		userEntity.setLastLogin(timeStamp);
		userEntity.setHasRole(role);
		userEntity.setPassword(customAuthentication.makeMd5Hash(
				timeStamp.getTime(), pass));
		userEntity.setUserId(timeStamp.getTime());
		userAuthService.createAnUpdateAuthUser(userEntity);
		securityException.setUserEntity(userEntity);
		securityException.setResponseCode(700);
		securityException.setError("sign in success");
		return securityException;
	}

	public SecurityExcepCustom authenticate_login(String username, String pass,
			String role) {
		SecurityExcepCustom securityException = new SecurityExcepCustom();
		UserEntity userEntity = userAuthService.getUserByEmail(username);
		securityException.setUserEntity(userEntity);
		if (userEntity == null) {
			securityException.setHasError(true);
			securityException.setResponseCode(900);
			securityException.setError("User not found with this email");
			return securityException;
		}
		if (!userEntity.getPassword().equals(customAuthentication.makeMd5Hash(
				userEntity.getJoinedDate().getTime(), pass))) {
			securityException.setHasError(true);
			securityException.setResponseCode(800);
			securityException.setError("Wrong Password");
		}

		Calendar calendar = Calendar.getInstance();
		Timestamp timeStamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		userEntity.setLastLogin(timeStamp);
		userAuthService.createAnUpdateAuthUser(userEntity);
		securityException.setResponseCode(700);
		securityException.setError("Login in success");

		return securityException;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	public void saveforgotPassword(String email) {
		ForgotPasswordEntity forgotPasswordEntity = new ForgotPasswordEntity();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, 1);
		Timestamp timeStamp = new java.sql.Timestamp(calendar.getTime()
				.getTime());
		forgotPasswordEntity.setExpire(timeStamp);
		forgotPasswordEntity.setEmailId(email);
		forgotPasswordEntity.setHash(customAuthentication.makeMd5Hash(
				timeStamp.getTime(), email));
		userAuthService.forgotPasswordSaveInDb(forgotPasswordEntity);
	}

	public ForgotPasswordEntity getforgotPassword(String hash) {

		return userAuthService.getforgotEntityByHash(hash);
	}

	public void changePasswordAndDeleteByEmailId(String hash, String newPassword) {
		ForgotPasswordEntity forgotPasswordEntity = userAuthService
				.getforgotEntityByHash(hash);
		UserEntity userEntity = userAuthService
				.getUserByEmail(forgotPasswordEntity.getEmailId());
		userEntity.setPassword(customAuthentication.makeMd5Hash(userEntity
				.getJoinedDate().getTime(), newPassword));
		userAuthService.createAnUpdateAuthUser(userEntity);
		userAuthService.deletGeneratedHashByEmailId(userEntity.getEmailId());
	}

	public Boolean validateHashAndNotExpired(String hash) {
		// TODO Auto-generated method stub
		ForgotPasswordEntity forgotPasswordEntity = userAuthService
				.getforgotEntityByHash(hash);
		if (forgotPasswordEntity != null) {
			Calendar calendar = Calendar.getInstance();
			Timestamp timeStamp = new java.sql.Timestamp(calendar.getTime()
					.getTime());
			Timestamp is_exp = forgotPasswordEntity.getExpire();
			if (is_exp.after(timeStamp) == true) {
				return true;
			}
		}
		return false;

	}

	public UserEntity getUserByEmailId(String email) {

		return userAuthService.getUserByEmail(email);
	}
}
