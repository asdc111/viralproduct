package com.viralproduct.security.excep;

import java.io.Serializable;

import org.springframework.security.core.Authentication;

import com.viralproduct.security.custom.entity.UserEntity;

public class SecurityExcepCustom implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String error;
	private boolean hasError=false;
	private UserEntity  userEntity;
	
	public UserEntity getUserEntity() {
		return userEntity;
	}
	public void setUserEntity(UserEntity userEntity) {
		this.userEntity = userEntity;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	private Integer responseCode;

	public boolean isHasError() {
		return hasError;
	}
	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	

}
