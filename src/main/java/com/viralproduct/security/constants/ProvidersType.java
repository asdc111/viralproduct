package com.viralproduct.security.constants;


public enum ProvidersType {
	FACEBOOK("FBK"),
	GOOGLE("GGL"),
	TWITTER("TWT"),
	OPENID("OID");
	

	private String code;
	
	private ProvidersType(String code) {
		this.code = code;
	}

	public String code() {
		return this.code;
	}

}
