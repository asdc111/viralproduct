package com.viralproduct.security.constants;

public enum UserType {
	ADMIN("ROLE_ADM"), PUBLISHER("ROLE_PUB"), USER("ROLE_USR");

	private String code;

	private UserType(String code) {
		this.code = code;
	}

	public String code() {
		return this.code;
	}

}
