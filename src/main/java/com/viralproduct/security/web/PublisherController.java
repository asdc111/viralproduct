package com.viralproduct.security.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viralproduct.security.constants.UserType;
import com.viralproduct.security.custom.CustomAuthenticationProvider;
import com.viralproduct.security.custom.entity.NotificationEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.excep.SecurityExcepCustom;
import com.viralproduct.security.model.Role;
import com.viralproduct.service.CacheService;
import com.viralproduct.service.PublisherService;

@Controller
@RequestMapping("/publisher")
public class PublisherController {

	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;

	@Autowired
	private PublisherService publisherService;

	@Autowired
	private CacheService cacheService;

	//
	// @RequestMapping(value = "/login", method = RequestMethod.GET)
	// protected String publisherLogin(HttpServletRequest request,
	// HttpServletResponse response, ModelMap model)
	// throws ServletException, IOException {
	// return "publisher_login";
	//
	// }

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	protected String publisherLogin(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication()
						.isAuthenticated()) {
			System.out.println(SecurityContextHolder.getContext()
					.getAuthentication().getAuthorities());
			return "publisher_dashboard";

		} else {
			response.sendRedirect(request.getContextPath() + "/publisher/login");
		}
		return null;
	}

	@RequestMapping(value = "/writeinfile", method = RequestMethod.GET)
	@ResponseBody
	public String writeInFile(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException {
		NotificationEntity notificationEntity = new NotificationEntity();
		notificationEntity.setSocialType(request.getParameter("answerOne"));
		notificationEntity.setNumberOfTweetAndPost(Integer.parseInt(request
				.getParameter("answerTwo")));
		notificationEntity.setContent(request.getParameter("answerThree"));
		publisherService.saveNotObject(notificationEntity);
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(notificationEntity);
		// Convert object to JSON string and pretty print
		jsonInString = mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(notificationEntity);
		
		cacheService.publishMessageUsingChannelName("pubsub", jsonInString);

		return "Done";

	}
}
