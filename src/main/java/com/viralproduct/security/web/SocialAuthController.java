package com.viralproduct.security.web;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.viralproduct.security.constants.UserType;
import com.viralproduct.security.custom.TwitterAuthenticationProvider;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.model.Role;
import com.viralproduct.security.model.SociailUserDetail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.User;

@Controller
@RequestMapping("/auth/")
public class SocialAuthController extends HttpServlet {
	private static final long serialVersionUID = -6205814293093350242L;

	@Autowired
	private TwitterAuthenticationProvider twitterAuthenticationProvider;
	
	
	
	@RequestMapping(value = "twitter/login", method = RequestMethod.GET)
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey("yPLoQ54CA7iZZNaJIDebRNsCs");
		builder.setOAuthConsumerSecret("MIGLz3Dcrtij8bHRGG76FafosYEDERHYNb4zBavTyfvGiiMyu9");
		Configuration configuration = builder.build();
		TwitterFactory factory = new TwitterFactory(configuration);
		Twitter twitter = factory.getInstance();
		request.getSession().setAttribute("twitter", twitter);
		try {

			StringBuffer callbackURL = request.getRequestURL();
			int index = callbackURL.lastIndexOf("/");
			callbackURL.replace(index, callbackURL.length(), "").append(
					"auth/twitter/callback");
			RequestToken requestToken = twitter
					.getOAuthRequestToken(callbackURL.toString());
			request.getSession().setAttribute("requestToken", requestToken);
			response.sendRedirect(requestToken.getAuthenticationURL());

		} catch (TwitterException e) {
			throw new ServletException(e);
		}
		
	}

	@RequestMapping(value = "twitter/callback", method = RequestMethod.GET)
	protected void callback(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			TwitterException {
		Twitter twitter = (Twitter) request.getSession()
				.getAttribute("twitter");
		RequestToken requestToken = (RequestToken) request.getSession()
				.getAttribute("requestToken");

		String auth_verifier = request.getParameter("oauth_verifier");

		AccessToken accessToken = null;
		accessToken = twitter.getOAuthAccessToken(requestToken, auth_verifier);
		request.getSession().removeAttribute("requestToken");
		request.getSession().setAttribute("access_token", accessToken);
		request.getSession().setAttribute("is_auth", true);

		try {
			twitter4j.User userTwitter = twitter.verifyCredentials();
			request.getSession().setAttribute("social_user_twitter_data",
					userTwitter);
			Role r = new Role();
			r.setName(UserType.USER.code());
			List<Role> roles = new ArrayList<Role>();
			roles.add(r);
			SecurityContext initialSecurityContext = SecurityContextHolder
					.getContext();
			User user = new User(userTwitter.getScreenName(), "@password",
					false, false, false, false, roles);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					user.getUsername(), user.getPassword(),
					user.getAuthorities());
			token.setDetails(user);
			twitterAuthenticationProvider.customAuthenticationMethod(
					userTwitter, token, accessToken);
			initialSecurityContext.setAuthentication(token);
			SecurityContextHolder.setContext(initialSecurityContext);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect(request.getContextPath() + "/user/dashboard");
	}
	
	
}
