package com.viralproduct.security.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import twitter4j.TwitterException;

import com.viralproduct.security.constants.UserType;
import com.viralproduct.security.custom.CustomAuthenticationProvider;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.excep.SecurityExcepCustom;
import com.viralproduct.security.model.Role;
import com.viralproduct.service.CacheService;

@Controller
@RequestMapping("/auth/")
public class AuthController {

	@Autowired
	private CustomAuthenticationProvider customAuthenticationProvider;
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException {
		

		HttpSession session = request.getSession();
		Boolean is_auth = false;
		return "login_page";

	}

	@RequestMapping(value = "signup", method = RequestMethod.GET)
	protected String publisherSignup(HttpServletRequest request,
			HttpServletResponse response, ModelMap model)
			throws ServletException, IOException {

		return "signup";
	}

	@RequestMapping(value = "authsign", method = RequestMethod.POST)
	@ResponseBody
	protected void publisherSignup(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("email") String email,
			@ModelAttribute("pass") String pass,
			@ModelAttribute("conf_pass") String confPass) throws IOException {
		System.out.println("help");
		SecurityExcepCustom securityExcepCustom = customAuthenticationProvider
				.authenticate_signUp(email, pass, confPass,
						UserType.PUBLISHER.code());
		if (securityExcepCustom.isHasError() == false) {
			UserEntity userEntity = securityExcepCustom.getUserEntity();
			Role r = new Role();
			r.setName(UserType.PUBLISHER.code());
			List<Role> roles = new ArrayList<Role>();
			roles.add(r);
			SecurityContext initialSecurityContext = SecurityContextHolder
					.getContext();
			User user = new User(userEntity.getUserId(),
					userEntity.getPassword(), false, false, false, false, roles);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					user.getUsername(), user.getPassword(),
					user.getAuthorities());
			token.setDetails(user);
			initialSecurityContext.setAuthentication(token);
			SecurityContextHolder.setContext(initialSecurityContext);

		}
		response.sendRedirect(request.getContextPath() + "/publisher/dashboard");

	}

	@RequestMapping(value = "authlogin", method = RequestMethod.POST)
	@ResponseBody
	protected void publisherLogin(HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute("email") String email,
			@ModelAttribute("pass") String pass) throws IOException {

		SecurityExcepCustom securityExcepCustom = customAuthenticationProvider
				.authenticate_login(email, pass, UserType.PUBLISHER.code());
		if (securityExcepCustom.isHasError() == false) {
			UserEntity userEntity = securityExcepCustom.getUserEntity();
			Role r = new Role();
			r.setName(UserType.PUBLISHER.code());
			List<Role> roles = new ArrayList<Role>();
			roles.add(r);
			SecurityContext initialSecurityContext = SecurityContextHolder
					.getContext();
			User user = new User(userEntity.getUserId(),
					userEntity.getPassword(), false, false, false, false, roles);
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					user.getUsername(), user.getPassword(),
					user.getAuthorities());
			token.setDetails(user);
			initialSecurityContext.setAuthentication(token);
			SecurityContextHolder.setContext(initialSecurityContext);
		}
		response.sendRedirect(request.getContextPath() + "/publisher/dashboard");
	}

	@RequestMapping(value = "forgotpassword", method = RequestMethod.GET)
	protected String publisherForgotPasswordjsp(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("email") String email) {

		return "forgot_password";

	}

	@RequestMapping(value = "forgotPasswordAuth", method = RequestMethod.POST)
	@ResponseBody
	protected void publisherForgotPassword(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("email") String email)
			throws IOException {
		UserEntity userEntity = customAuthenticationProvider
				.getUserByEmailId(email);
		if (userEntity != null) {
			customAuthenticationProvider.saveforgotPassword(email);
			response.sendRedirect(request.getContextPath()
					+ "/publisher/forgotSuccess");
		} else {
			response.sendRedirect(request.getContextPath()
					+ "/publisher/forgotError");
		}
	}

	@RequestMapping(value = "forgotError", method = RequestMethod.GET)
	@ResponseBody
	protected String publisherForgotPasswordError(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		return "Error";
	}

	@RequestMapping(value = "forgotSuccess", method = RequestMethod.GET)
	@ResponseBody
	protected String publisherForgotPasswordSuccess(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		return "Success";
	}

	@RequestMapping(value = "changepassword", method = RequestMethod.GET)
	protected String setNewPasswordjsp(HttpServletRequest request,
			HttpServletResponse response, @RequestParam("hash") String hash,
			ModelMap model) throws IOException {
		if (!(hash != null && customAuthenticationProvider
				.validateHashAndNotExpired(hash)) == true) {
			response.sendRedirect(request.getContextPath()
					+ "/publisher/forgotError");
		}
		model.addAttribute("hash", hash);
		return "set_new_password";
	}

	@RequestMapping(value = "changePasswordAuth", method = RequestMethod.POST)
	protected void publisherChangeForgotPassword(HttpServletRequest request,
			HttpServletResponse response, @ModelAttribute("pass") String pass,
			@ModelAttribute("newPass") String newPass,
			@ModelAttribute("hash") String hash) throws IOException {
		if (pass != null && newPass != null && !pass.equals(newPass)) {
			response.sendRedirect(request.getContextPath()
					+ "/publisher/forgotError");
		}
		customAuthenticationProvider.changePasswordAndDeleteByEmailId(hash,
				pass);
		response.sendRedirect(request.getContextPath() + "/publisher/dashboard");
	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String userLogout(HttpServletRequest request,
			HttpServletResponse response, ModelMap model)
			throws TwitterException, IOException {

		return "login_page";

	}
}
