package com.viralproduct.security.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.viralproduct.service.CacheService;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

@Controller
@RequestMapping("/")
public class UserController {
	private static final Logger logger = Logger.getLogger(UserController.class);

	

	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException {
		HttpSession session = request.getSession();
		Boolean is_auth = false;
		// is_auth =
		// SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication()
						.isAuthenticated()) {
			response.sendRedirect(request.getContextPath() + "/user/dashboard");
		}
		return "login_page";

	}

	@RequestMapping(value = "user/dashboard", method = RequestMethod.GET)
	public String UserPage(HttpServletRequest request,
			HttpServletResponse response, ModelMap model)
			throws TwitterException, IOException {
		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication()
						.isAuthenticated()) {
			Twitter twitter = (Twitter) request.getSession().getAttribute(
					"twitter");
			List<Status> statuses = twitter.getHomeTimeline();
			System.out.println("Showing home timeline.");
			System.out.println(twitter.verifyCredentials().getId());
			for (Status status : statuses) {
				System.out.println(status.getUser().getName() + ":"
						+ status.getText());
			}
			return "user_page";
		} else {
			response.sendRedirect(request.getContextPath() + "/login");
		}
		return null;

	}
}
