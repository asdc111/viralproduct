package com.viralproduct.security.model;

import java.io.Serializable;

public class SociailUserDetail  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String uid;
	private String appId ;
	private String userSecretKey;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getUserSecretKey() {
		return userSecretKey;
	}
	public void setUserSecretKey(String userSecretKey) {
		this.userSecretKey = userSecretKey;
	}
	

}
