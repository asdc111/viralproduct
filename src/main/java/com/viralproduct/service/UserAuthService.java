package com.viralproduct.service;

import org.springframework.stereotype.Service;

import com.viralproduct.security.custom.entity.ForgotPasswordEntity;
import com.viralproduct.security.custom.entity.UserEntity;
import com.viralproduct.security.custom.entity.UserSocialAccountEntity;

@Service("userAuthService")
public interface UserAuthService {

	UserEntity createAnUpdateAuthUser(UserEntity userEntity);
	UserSocialAccountEntity getOrCreateSocialUser(UserSocialAccountEntity userSocialAccountEntity);
	UserEntity getUserByEmail(String username);
	void forgotPasswordSaveInDb(ForgotPasswordEntity forgotPasswordEntity);
	ForgotPasswordEntity getforgotEntityByHash(String hash);
	void deletGeneratedHashByEmailId(String email);
	
	
	
	
}
