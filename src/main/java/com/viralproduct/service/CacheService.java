package com.viralproduct.service;

import org.springframework.stereotype.Service;



@Service("cacheService")
public interface CacheService {

	String getValue(String Key);

	void setValue(String Key, String Value);

	boolean isExist(String Key);

	boolean publishMessageUsingChannelName(String channel, String message);
	
}
